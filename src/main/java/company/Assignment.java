package company;

import company.config.ApplicationConfig;
import company.config.CommandLineArgsParser;
import company.config.ArgumentException;
import company.comparator.Comparator;
import company.comparator.ComparatorFactory;
import company.path.PathPrinterXML;
import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.regex.PatternSyntaxException;

public class Assignment {

    public static void main(String[] args) {

        ApplicationConfig config = new ApplicationConfig();
        CommandLineArgsParser argsParser = new CommandLineArgsParser();

        try {
            argsParser.parseArguments(args, config);

            Comparator comparator = ComparatorFactory.getComparator(config.getSearchType(), config.getSearchString());

            PathPrinterXML printer = new PathPrinterXML(comparator);
            printer.printFromFile(config.getFileName());
        }
        catch (ArgumentException e){
            System.err.println(e.getMessage());
            System.exit(1);
        }
        catch (ParserConfigurationException | IOException | SAXException e) {
            System.err.println("Error: " + e.getMessage());
            System.exit(1);
        }
        catch (PatternSyntaxException e){
            System.err.println("Not valid regular expression: " + e.getMessage());
            System.exit(1);
        }
    }
}
