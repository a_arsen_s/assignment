package company.comparator;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.PathMatcher;

public class ExtendedComparator extends Comparator {

    private final PathMatcher matcher;

    public ExtendedComparator(String searchExpression){
        matcher = FileSystems.getDefault().getPathMatcher("regex:" + searchExpression);
    }

    @Override
    public boolean compare(String name) {
        return matcher.matches(Path.of(name));
    }
}
