package company.comparator;

public abstract class Comparator {

    public abstract boolean compare(String name);

}
