package company.comparator;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.PathMatcher;

public class SimpleComparator extends Comparator {

    private final PathMatcher matcher;

    public SimpleComparator(String searchExpression){
        matcher = FileSystems.getDefault().getPathMatcher("glob:" + searchExpression);
    }

    @Override
    public boolean compare(String name) {
        return matcher.matches(Path.of(name));
    }
}
