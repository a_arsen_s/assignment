package company.integration;

import company.Assignment;
import company.TestConsoleOutput;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.util.List;

import static company.constant.Constant.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Integration test")
class AssignmentTest extends TestConsoleOutput {

    private final String FILE_NAME = "src/test/resources/test.xml";

    @DisplayName("Test with empty comparator")
    @Test
    void testWithEmptyComparator() {

        String[] args = {KEY_INPUT_FILE, FILE_NAME};
        Assignment.main(args);

        List<String> expectedValue = List.of(
                "/file-776194140.xml",
                "/dir-880176375/file-1073842118.java",
                "/dir-880176375/dir-2145307015/file-1498940214.xhtml"
        );

        List<String> actualValue = getConsoleOutput();

        assertEquals(expectedValue, actualValue);
    }

    @DisplayName("Test with exact comparator")
    @Test
    void testWithExactComparator() {

        String[] args = {KEY_INPUT_FILE, FILE_NAME, KEY_MASK, "file-1498940214.xhtml"};
        Assignment.main(args);

        List<String> expectedValue = List.of(
                "/dir-880176375/dir-2145307015/file-1498940214.xhtml"
        );

        List<String> actualValue = getConsoleOutput();

        assertEquals(expectedValue, actualValue);
    }

    @DisplayName("Test with simple comparator")
    @Test
    void testWithSimpleComparator() {

        String[] args = {KEY_INPUT_FILE, FILE_NAME, KEY_MASK, "*.java"};
        Assignment.main(args);

        List<String> expectedValue = List.of(
                "/dir-880176375/file-1073842118.java"
        );

        List<String> actualValue = getConsoleOutput();

        assertEquals(expectedValue, actualValue);
    }

    @DisplayName("Test with extended comparator")
    @Test
    void testWithExtendedComparator() {

        String[] args = {KEY_INPUT_FILE, FILE_NAME, KEY_MASK_REGULAR, ".*?[a-z]{4}-\\d+\\.[a-z]+"};
        Assignment.main(args);

        List<String> expectedValue = List.of(
                "/file-776194140.xml",
                "/dir-880176375/file-1073842118.java",
                "/dir-880176375/dir-2145307015/file-1498940214.xhtml"
        );

        List<String> actualValue = getConsoleOutput();

        assertEquals(expectedValue, actualValue);
    }
}