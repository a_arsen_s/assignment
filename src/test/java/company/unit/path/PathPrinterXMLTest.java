package company.unit.path;

import company.TestConsoleOutput;
import company.comparator.EmptyComparator;
import company.comparator.ExtendedComparator;
import company.comparator.SimpleComparator;
import company.path.PathPrinterXML;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Path printer test")
class PathPrinterXMLTest extends TestConsoleOutput {

    private final String FILE_NAME = "src/test/resources/test.xml";

    @DisplayName("Test with empty comparator")
    @Test
    void testPrintFromFileWithEmptyComparator() throws IOException, SAXException, ParserConfigurationException {

        PathPrinterXML printer = new PathPrinterXML(new EmptyComparator());
        printer.printFromFile(FILE_NAME);

        List<String> expectedValue = List.of(
                "/file-776194140.xml",
                "/dir-880176375/file-1073842118.java",
                "/dir-880176375/dir-2145307015/file-1498940214.xhtml"
        );

        List<String> actualValue = getConsoleOutput();

        assertEquals(expectedValue, actualValue);
    }

    @DisplayName("Test with exact comparator")
    @Test
    void testPrintFromFileWithExactComparator() throws IOException, SAXException, ParserConfigurationException {

        PathPrinterXML printer = new PathPrinterXML(new SimpleComparator("file-1498940214.xhtml"));
        printer.printFromFile(FILE_NAME);

        List<String> expectedValue = List.of(
                "/dir-880176375/dir-2145307015/file-1498940214.xhtml"
        );

        List<String> actualValue = getConsoleOutput();

        assertEquals(expectedValue, actualValue);
    }

    @DisplayName("Test with simple comparator")
    @Test
    void testPrintFromFileWithSimpleComparator() throws IOException, SAXException, ParserConfigurationException {

        PathPrinterXML printer = new PathPrinterXML(new SimpleComparator("*.java"));
        printer.printFromFile(FILE_NAME);

        List<String> expectedValue = List.of(
                "/dir-880176375/file-1073842118.java"
        );

        List<String> actualValue = getConsoleOutput();

        assertEquals(expectedValue, actualValue);
    }

    @DisplayName("Test with extended comparator")
    @Test
    void testPrintFromFileWithExtendedComparator() throws IOException, SAXException, ParserConfigurationException {

        PathPrinterXML printer = new PathPrinterXML(new ExtendedComparator(".*?[a-z]{4}-\\d+\\.[a-z]+"));
        printer.printFromFile(FILE_NAME);

        List<String> expectedValue = List.of(
                "/file-776194140.xml",
                "/dir-880176375/file-1073842118.java",
                "/dir-880176375/dir-2145307015/file-1498940214.xhtml"
        );

        List<String> actualValue = getConsoleOutput();

        assertEquals(expectedValue, actualValue);
    }

}