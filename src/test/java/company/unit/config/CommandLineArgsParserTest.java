package company.unit.config;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import company.config.ArgumentException;
import company.config.CommandLineArgsParser;
import company.constant.TypeOfCompare;
import company.config.ApplicationConfig;
import org.junit.jupiter.api.*;

@DisplayName("Command line arguments parsing")
class CommandLineArgsParserTest {

    private final ApplicationConfig     appConfig       = new ApplicationConfig();
    private final CommandLineArgsParser argumentsParser = new CommandLineArgsParser();

    @DisplayName("Test with empty input")
    @Test
    void testEmptyCliInput() {
        assertThrows(ArgumentException.class, () -> argumentsParser.parseArguments(new String[0], appConfig));
    }

    @DisplayName("Test with unknown cli option")
    @Test

    void testUnknownCliOption() {

        String[] commandLineArgs = {"-t", "new.xml"};
        assertThrows(ArgumentException.class, () -> argumentsParser.parseArguments(commandLineArgs, appConfig));

    }

    @DisplayName("Test with missed cli argument")
    @Test
    void testMissedCliArgument() {

        String[] commandLineArgs = {"-f"};
        assertThrows(ArgumentException.class, () -> argumentsParser.parseArguments(commandLineArgs, appConfig));

    }

    @DisplayName("Test with correct cli arguments")
    @Test
    void testCorrectCliArgument() {

        String[] commandLineArgs = {"-f", "new.xml", "-s", "*.java"};

        try {
            argumentsParser.parseArguments(commandLineArgs, appConfig);

            assertEquals("new.xml",     appConfig.getFileName());
            assertEquals("*.java",      appConfig.getSearchString());
            assertEquals(TypeOfCompare.SIMPLE,   appConfig.getSearchType());

        } catch (ArgumentException e) {
            fail(e.getMessage());
        }
    }
}