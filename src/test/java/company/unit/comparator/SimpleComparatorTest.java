package company.unit.comparator;

import company.comparator.Comparator;
import company.comparator.SimpleComparator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

@DisplayName("Simple comparator test")
class SimpleComparatorTest {

    @DisplayName("Test correct search expressions")
    @ParameterizedTest
    @ValueSource(strings = {"*.java", "1.txt", "", "file?5*"})
    void testSetCorrectMatcher(String searchString) {

        Assertions.assertDoesNotThrow(() ->  new SimpleComparator(searchString));

    }

    @DisplayName("Test compare method")
    @ParameterizedTest
    @CsvSource({"file-*.java,           file-65455-test.java",
                "file-44336-test.java,  file-44336-test.java"})
    void testCompare(String searchString, String stringToCheck) {

        Comparator comparator = new SimpleComparator(searchString);

        boolean actualValue = comparator.compare(stringToCheck);

        Assertions.assertTrue(actualValue);

    }

}