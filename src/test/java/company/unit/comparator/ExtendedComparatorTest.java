package company.unit.comparator;

import company.comparator.Comparator;
import company.comparator.ExtendedComparator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import java.util.regex.PatternSyntaxException;


@DisplayName("Extended comparator test")
class ExtendedComparatorTest {

    @DisplayName("Test correct search expressions")
    @ParameterizedTest
    @ValueSource(strings = {".*.java", "1.txt", ""})
    void testSetCorrectMatcher(String searchString) {

        Assertions.assertDoesNotThrow(() -> new ExtendedComparator(searchString));

    }

    @DisplayName("Test incorrect search expressions")
    @ParameterizedTest
    @ValueSource(strings = {"*.java"})
    void testSetIncorrectMatcher(String searchString) {

        Assertions.assertThrows(PatternSyntaxException.class, () -> new ExtendedComparator(searchString));

    }

    @DisplayName("Test compare method")
    @ParameterizedTest
    @CsvSource({".*.java,                  file-65455-test.java",
                "[a-zA-Z]{4}-\\d+-.*.java, file-36324-test.java"})
    void testCompare(String searchString, String stringToCheck) {

        Comparator comparator = new ExtendedComparator(searchString);

        boolean actualValue = comparator.compare(stringToCheck);

        Assertions.assertTrue(actualValue);

    }
}