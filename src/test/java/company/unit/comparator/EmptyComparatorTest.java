package company.unit.comparator;

import company.comparator.Comparator;
import company.comparator.EmptyComparator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("Empty comparator test")
class EmptyComparatorTest {

    @DisplayName("Test compare method")
    @Test
    void testCompare() {

        Comparator comparator = new EmptyComparator();

        boolean actualValue = comparator.compare("Test");

        Assertions.assertTrue(actualValue);

    }

}