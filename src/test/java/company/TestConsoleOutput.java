package company;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.stream.Collectors;

public abstract class TestConsoleOutput {

    private final  ByteArrayOutputStream OUT_CONTENT  = new ByteArrayOutputStream();
    private final  PrintStream           ORIGINAL_OUT = System.out;

    @BeforeEach
    void setUpStreams() {
        System.setOut(new PrintStream(OUT_CONTENT));
    }

    @AfterEach
    void restoreStreams() {
        System.setOut(ORIGINAL_OUT);
    }

    public List<String> getConsoleOutput(){
        return OUT_CONTENT.toString().lines().collect(Collectors.toList());
    }
}
